```sh
$ conda create -n project_lijia python=3.7.6
$ conda activate project_lijia
(project_lijia) $ pip install django==2.2.12
(project_lijia) $ pip install -r requirements.txt
(project_lijia) $ django-admin startproject project_lijia
(project_lijia) $ python manage.py startapp app_index
(project_lijia) $ python manage.py runserver
```

```sh
$ heroku login
$ heroku create lijia-s0713-sample
$ heroku config:set SECRET_KEY='**************************************************'
$ git push heroku master
$ heroku run python manage.py migrate
$ heroku open
```
