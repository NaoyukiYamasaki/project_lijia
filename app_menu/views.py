from django.shortcuts import render
from django.http import HttpResponse
from .models import Menu

def menu(request):

    menu = Menu()
    menu_items = Menu.objects.all()
    return render(request, "menu.html", {"menu_items": menu_items})

# def db(request):

#     greeting = Greeting()
#     greeting.save()

#     greetings = Greeting.objects.all()

#     return render(request, "db.html", {"greetings": greetings})
