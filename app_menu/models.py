from django.db import models
import sys
sys.path.append('..')

heroku = False
try:
    import local_tag
except ImportError:
    heroku = True

class Menu(models.Model):
    
    title = models.CharField('料理名', max_length=255, null=True)
    price = models.IntegerField('価格(円)', default=1000, null=True)
    description = models.TextField('説明', null=True)
    category = models.IntegerField('カテゴリー', default=0, null=True)
    if heroku:
        photo = models.ImageField('写真', null=True)
    else:
        photo = models.ImageField('写真', upload_to='menu/', null=True)
    created_at = models.DateTimeField("登録日", auto_now_add=True, null=True)

    class Meta:
        verbose_name_plural = 'Menu_items'

    def __str__(self):
        return self.title
