from django.urls import path, include
from django.contrib import admin
admin.autodiscover()
import app_index.views, app_menu.views

urlpatterns = [
    path("", app_index.views.index, name="index"),
    path("menu/", app_menu.views.menu, name="menu"),
    path("admin/", admin.site.urls),
]

import sys
sys.path.append('..')

heroku = False
try:
    import local_tag
except ImportError:
    heroku = True

if not heroku:
  # localで必要？
    from . import settings
    from django.contrib.staticfiles.urls import static
    from django.contrib.staticfiles.urls import staticfiles_urlpatterns
 
    urlpatterns += staticfiles_urlpatterns()
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

#おそらく不要
# if heroku:
#     from django.urls import re_path
#     from django.views.static import serve
#     urlpatterns = [
#         re_path(r'^media/(?P<path>.*)$', serve, {'document_root': settings.MEDIA_ROOT}),
#     ]