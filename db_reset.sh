#!/bin/sh
# $ sh db_reset.sh app_menu

cd app_menu
rm -d -r migrations/
cd ..
rm -d -r db.sqlite3
rm -d -r media/menu/
python manage.py makemigrations app_menu
python manage.py migrate
python manage.py createsuperuser --username admin  --email s0713_yamasaki@yahoo.co.jp
python manage.py runserver
